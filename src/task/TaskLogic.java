package task;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class TaskLogic {
	// =========Declarations=========
	public WebDriver driver;
	public WebDriverWait wait;
	public int today;
	public List<WebElement> Dates;

	// =========Constructor=========
	public TaskLogic(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 30, 100);
	}

	// =========Locators=========
	/*
	 * Define all the locators that will be used all over the test here
	 * */
	By RTripTab = By.id("tab-roundTrip");
	By OriginCity = By.xpath("//input[@name = 'origin']");
	By frstOrigCity = By.xpath(
			"//div[@role='region'][1]/div[@class='el-scrollbar']//ul[@class='el-scrollbar__view el-autocomplete-suggestion__list']/li[1]/div");
	By DestCity = By.xpath("//input[@name = 'destination']");	
	By frstDestCity = By.xpath(
			"//div[@role='region'][2]/div[@class='el-scrollbar']//ul[@class='el-scrollbar__view el-autocomplete-suggestion__list']/li[1]/div");
	By CalIcon = By.xpath("//i[@class='el-input__icon el-range__icon el-icon-date']");
	By DepDate = By.xpath("//input[@name='d']");
	By ArrDate = By.xpath("//input[@name='a']");
	By SearchBtn = By.xpath("//button[contains(text(),'search now')]");
	By StopBtn = By.xpath("//button[contains(text(),'STOPS')]");
	By SearchResults = By.xpath("//div[@class='text-primary-third container lg:px-0 md:px-8 px-2']");
	By ModSearchBtn = By.xpath("//button[contains(text(),'Modify Search')]");
	By resultsBtns = By.xpath("//span[@class='float-left md:mb-0 mb-2']");
	By  signUpLink = By.xpath("//a[contains(text(),'Sign up')]");
	By frstName = By.xpath("//input[@placeholder='First Name']");
	By fmilyName = By.xpath("//input[@placeholder='Family Name']");
	By eMail = By.xpath("//input[@placeholder='john@example.com']");
	By pssWord = By.xpath("//input[@type='password']");
	By cAccntBtn = By.xpath("//button[contains(text(),'CREATE ACCOUNT')]");
	By hiMsg = By.xpath("//span[contains(text(),'Hi,')]");
	By verfyLink = By.xpath("//span[contains(text(),'Verify your account')]");
	By  contactUsLink = By.xpath("//a[contains(text(),'Contact Us')]");
	By fullName = By.xpath("//input[@placeholder='Full name']");
	By ticketMail = By.xpath("//input[@placeholder='example@email.com']");
	By catSelect = By.xpath("//input[@placeholder='Select Category']");
	By otherCat = By.xpath("//span[contains(text(),'Other')]");
	//By ticketMessage = By.xpath("//input[@placeholder='Write your message here �']");
	By ticketMessage = By.tagName("textarea");
	By sendBtn = By.xpath("//button[contains(text(),'SEND')]");
	By thanksForContact = By.xpath("//h3[@class='text-xl pt-5']");
	
	
//=========================================
	public void RTrip(String org, String dest , String dfrom , String dto) throws Exception {
		
	
		driver.findElement(RTripTab).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(OriginCity));
		driver.findElement(OriginCity).click();
		driver.findElement(OriginCity).sendKeys(org);
		if(driver.findElements(frstOrigCity).isEmpty())
		{
		wait.until(ExpectedConditions.elementToBeClickable(frstOrigCity));
		driver.findElement(frstOrigCity).click();
		}
		
		wait.until(ExpectedConditions.elementToBeClickable(DestCity));
		driver.findElement(DestCity).click();
		driver.findElement(DestCity).sendKeys(dest);
		if(driver.findElements(frstDestCity).isEmpty())
		{
		wait.until(ExpectedConditions.elementToBeClickable(frstDestCity));
		driver.findElement(frstDestCity).click();
		}
		
		wait.until(ExpectedConditions.elementToBeClickable(RTripTab));
		driver.findElement(RTripTab).click();
		
		
		wait.until(ExpectedConditions.elementToBeClickable(DepDate));
		//driver.findElement(DepDate).click();
		driver.findElement(DepDate).sendKeys(dfrom);
		driver.findElement(ArrDate).sendKeys(dto);
		driver.findElement(RTripTab).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(SearchBtn));
		driver.findElement(SearchBtn).click();
		wait.until(ExpectedConditions.presenceOfElementLocated(resultsBtns));
		Assert.assertFalse(driver.findElements(resultsBtns).isEmpty());

	}
//=========================================
	public void SignUp(String firstName, String familyName, String email, String pWord) throws InterruptedException
	{
		driver.navigate().to("https://www.fly365.com/");
		JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
		javascriptExecutor.executeScript("arguments[0].scrollIntoView(true);", driver.findElement(signUpLink));
		driver.findElement(signUpLink).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(frstName));
		driver.findElement(frstName).sendKeys(firstName);
		
		wait.until(ExpectedConditions.elementToBeClickable(fmilyName));
		driver.findElement(fmilyName).sendKeys(familyName);
		
		wait.until(ExpectedConditions.elementToBeClickable(eMail));
		driver.findElement(eMail).sendKeys(email);
		
		wait.until(ExpectedConditions.elementToBeClickable(pssWord));
		driver.findElement(pssWord).sendKeys(pWord);
		
		wait.until(ExpectedConditions.elementToBeClickable(cAccntBtn));
		driver.findElement(cAccntBtn).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(hiMsg));
		String hmsg = driver.findElement(hiMsg).getText();
		System.out.println(hmsg);
		Assert.assertEquals(hmsg, "Hi,");
		
		wait.until(ExpectedConditions.elementToBeClickable(verfyLink));
		driver.findElement(verfyLink).click();	
		
		//Thread.sleep(2000);
		
	}
//=========================================
	public void CreateTicket(String fuName, String tMail, String ticektM) throws Exception
	{
		driver.navigate().to("https://www.fly365.com/");
		JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
		javascriptExecutor.executeScript("arguments[0].scrollIntoView(true);", driver.findElement(contactUsLink));
		driver.findElement(contactUsLink).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(fullName));
		driver.findElement(fullName).sendKeys(fuName);
		
		wait.until(ExpectedConditions.elementToBeClickable(ticketMail));
		driver.findElement(ticketMail).sendKeys(tMail);
		
		wait.until(ExpectedConditions.elementToBeClickable(catSelect));
		driver.findElement(catSelect).click();
	
		wait.until(ExpectedConditions.elementToBeClickable(otherCat));
		driver.findElement(otherCat).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(ticketMessage));
		driver.findElement(ticketMessage).sendKeys(ticektM);
		
		wait.until(ExpectedConditions.elementToBeClickable(sendBtn));
		driver.findElement(sendBtn).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(thanksForContact));
		String thanksMsg = driver.findElement(thanksForContact).getText();
		System.out.println(thanksMsg);
		Assert.assertEquals(thanksMsg, "Thank you for contacting us");
		//Thread.sleep(2000);
	}
}



