package task;

import java.io.FileReader;
import java.util.List;

import au.com.bytecode.opencsv.CSVReader;

public class TaskHelper {
	//reading the data from a CSV file based on the "OpenCSV" library
		public Object[][] csvFileReader(String relPath) throws Exception{
			CSVReader readunamepass= new CSVReader(new FileReader(relPath));
			List<String[]> list = readunamepass.readAll();
			String[][] array = new String[list.size()][];
			for(int i=0;i<list.size();i++)
			{
			array[i] = list.get(i);
				}
			return array;
			}

}
