package task;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TaskTests {
	
	public WebDriver driver;
// =======================================================	
	@BeforeTest
	public void beforeTest() throws Exception {
		
		System.out.println("@BeforeTest:Test Initialization");
		
		ChromeOptions options = new ChromeOptions();
		System.setProperty("webdriver.chrome.driver", "Libs/chromedriver.exe");
		/*setting "Chrome Driver" path, the used Chrome version is 75*/		
		driver = new ChromeDriver(options);//Starting an instance from Chrome driver
		driver.manage().window().maximize();
		driver.get("https://www.fly365.com/");//Navigate to Fly365
		Thread.sleep(100);
	}
// =======================================================
	@AfterTest
	public void afterTest() {
		System.out.println("@AfterTest:Test Teardown");
		driver.quit();//Killing the used driver instance
	}
// =======================================================
	@DataProvider(name = "searchParameters")
	public Object[][] SerachParameters() throws Exception
	{
		TaskHelper TH = new TaskHelper();
		return TH.csvFileReader("Libs/sParameters.csv");
	}
	@Test(priority = 1 , dataProvider = "searchParameters")
	public void RoundTripSearch(String origin, String destination , String from , String to) throws Exception {
		
		System.out.println("Test1:Search For A Roundtrip");
		TaskLogic TL1 = new TaskLogic(driver);
		TL1.RTrip(origin, destination, from, to);
	
	}
// =======================================================
	@DataProvider(name = "SignUpParameters")
	public Object[][] SignUpParameters() throws Exception
	{
		TaskHelper TH = new TaskHelper();
		return TH.csvFileReader("Libs/sUp.csv");
	}
	@Test(priority = 2, dataProvider = "SignUpParameters")
	public void SignUp(String first, String family, String mail, String pw) throws Exception {
		
		System.out.println("Test2: SigUp");
		TaskLogic TL2 = new TaskLogic(driver);	
		TL2.SignUp(first, family, mail, pw);
	}
// =======================================================
	@DataProvider(name = "TicketParameters")
	public Object[][] TicketParameters() throws Exception
	{
		TaskHelper TH = new TaskHelper();
		return TH.csvFileReader("Libs/tParameters.csv");
	}
	@Test(priority = 3, dataProvider = "TicketParameters")
	public void ContactUsTicket(String name, String mail, String message) throws Exception {
		
		System.out.println("Test3: Add a Contact Us Ticket");
		TaskLogic TL3 = new TaskLogic(driver);
		TL3.CreateTicket(name, mail, message);
		
	}
	

}
