**Prerequisites**

In order to be able to run the script, the machine has to got the following:

- Java JDK (Mandatory)
- Set the JAVA_HOME environment variable (Mandatory)
- Eclipse IDE (Optional)
- Eclipse TestNG plug-in (Mandatory only if the user will execute the script through the Eclipse IDE)
- Chrome (V 75 is the most stable version at the time of developing this script [In case of other browsers or other chrome versions the user has to work with the comptible web driver])

**Steps**

- Download the whole project resources (The whole "fly365" project files)
- Navigate to (["Project Folder"]/Libs)
- Modify the CSV files present in this folder as follows
    * For the "Round Trip Search" TC, use the file "sParameters.csv" (at which the user can set the origin city, destination city, arrival date & departure date) [for dates, please follow the same format in the file]
    * For the "SignUp" TC, use the file "sUp.csv" (at which the user can set the First Name, Family Name, Email & Password)[every time running the SignUp TC, the user has to provide a new email which wasn't used before for any user]
    Note: Only the "Signup" process was automated but the "Account Verification" wasn't
    * For the "Send Ticket" TC, use the file "tParameters.csv" (at which the user can set the Full Name, Email & Ticket message)

- If you are using Eclipse IDE
    * Import the project into your workspace
    * Right Click on the "src/task/TaskXML.xml"
    * Select (Run As >> TestNG Suite) --> the script (Test Cases) will be executed & the user can check the execution log through the Eclipse console)

- If you don't want to use the IDE & want to run the script through the CMD
    * Open the project folder
    * Right Click on "run.bat" file
    * Select "Run As Administrator" --> the script (Test Cases) will be executed & the user can check the execution log through the opened CMD window)

- To check the the test report
    * Navigate to (["Project Folder"]/test-output/Fly365AutomationTask/)[the folder will be generated only after executing the script]
    * Double click on the "Fly365Task.html" file to check the test results